﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeansOfficeApp.Models
{
    class Status
    {
        public string Date { get; set; }
        public string CurrentStatus { get; set; }
        public string Studies { get; set; }
    }
}
