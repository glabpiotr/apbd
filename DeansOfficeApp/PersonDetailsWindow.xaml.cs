﻿using DeansOfficeApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DeansOfficeApp
{
    /// <summary>
    /// Interaction logic for PersonDetailsWindow.xaml
    /// </summary>
    public partial class PersonDetailsWindow : Window
    {
        public PersonDetailsWindow()
        {
            InitializeComponent();

            Student testStudent = new Student { FirstName = "Jan", LastName = "Kowalski", IndexNumber = "s1234", Year = 2018, Semester = "2017/2018 letni", Specialization = "Bazy danych", Status = "Student", Notes = "Brak", Course = "Informatyka", Balance = -1200};
            Student testStudent1 = new Student { FirstName = "Mariusz", LastName = "Kowalski", IndexNumber = "s1234", Year = 2018, Semester = "2017/2018 letni", Specialization = "Bazy danych", Status = "Student", Notes = "Brak", Course = "Informatyka", Balance = 0 };
            Student testStudent2 = new Student { FirstName = "Andrzej", LastName = "Kowalski", IndexNumber = "s1234", Year = 2018, Semester = "2017/2018 letni", Specialization = "Bazy danych", Status = "Student", Notes = "Brak", Course = "Informatyka", Balance = 1200 };

           

            FillTestData(testStudent1);

            var statuses = new List<Status>();

            statuses.Add(new Status { Date = "Data od", CurrentStatus = "Status", Studies = "Studia" });
            statuses.Add(new Status { Date = "28-02-2017", CurrentStatus = "Student", Studies = testStudent1.Course});
            statuses.Add(new Status { Date = "28-02-2017", CurrentStatus = "Student", Studies = testStudent1.Course});

            Statuses.ItemsSource = statuses;
        }

        private void CloseMenuItem_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void MainMenuItem_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }

        private void FillTestData(Student student)
        {
            NameSurname.Content = student.LastName + " " + student.FirstName;
            Course.Content = student.Course;
            Speciality.Content = "Specjalizacja: " + student.Specialization;
            IndexNumber.Content = student.IndexNumber;
            CurrentYear.Content = student.Semester.Substring(0, 4);
            Semester.Content = student.Semester;
            Balance.Content = student.Balance;
            SetBalanceBackground(student.Balance);
            Pesel.Content = "90929229212";
            Gender.Content = "meżczyzna";
            BirthDate.Content = "1990-01-02";
            BirthCity.Content = "Warszawa";
            Citizenship.Content = "Polskie";
            Phone.Content = "544-334-223";
            Adress.Content = "Warszawa \nZłota 12 m. 5";
        }

        private void SetBalanceBackground(int balance)
        {
            if (balance < 0)
                Balance.Background = Brushes.Red;
            else if (balance > 0)
                Balance.Background = Brushes.Green;
            else
                Balance.Background = Brushes.White;
        }
    }
}
