﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cwiczenie3.Models
{
    public class Student
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string IndexNumber { get; set; }
    }
}
