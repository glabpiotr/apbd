﻿using Cwiczenie3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Cwiczenie3
{
    /// <summary>
    /// Interaction logic for Zaanie3i4.xaml
    /// </summary>
    public partial class Zaanie3i4 : Window
    {
        List<Student> studenci = new List<Student>();
        public Zaanie3i4()
        {
            InitializeComponent();
            studenci.Add(new Student { Name = "Jan", Surname = "Kowalski", IndexNumber = "s1234" });
            studenci.Add(new Student { Name = "Piotr", Surname = "Nowak", IndexNumber = "s4321" });

            StudentsListDataGrid.ItemsSource = studenci;

        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            Regex regex = new Regex(@"s\d{5}");
            Match match = regex.Match(NrIndeksuStudent.Text);
            if (match.Success)
            {
                var newStudent = new Student { Name = ImieStudent.Text, Surname = NazwiskoStudent.Text, IndexNumber = NrIndeksuStudent.Text };
                studenci.Add(newStudent);
                ImieStudent.Text = "";
                NazwiskoStudent.Text = "";
                NrIndeksuStudent.Text = "";
                StudentsListDataGrid.ItemsSource = null;
                StudentsListDataGrid.ItemsSource = studenci;
            } else
            {
                MessageBox.Show("Niepoprawna wartość numeru indeksu");
            }
           
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            var selectedIndex = StudentsListDataGrid.SelectedIndex;
            if (selectedIndex != -1)
            {
                studenci.RemoveAt(selectedIndex);
                StudentsListDataGrid.ItemsSource = null;
                StudentsListDataGrid.ItemsSource = studenci;
            } else
            {
                MessageBox.Show("Nie został wybrany żaden Student");
            }
        }

        private void StudentsListDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var windowEdit = new StudentEditDialog((Student) StudentsListDataGrid.SelectedItem);
            windowEdit.Show();
        }
    }
}
